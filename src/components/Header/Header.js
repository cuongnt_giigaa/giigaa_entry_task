import React, {Component} from "react";
import {Dropdown, Layout, Menu} from "antd";
import PropTypes from "prop-types";
import "./index.css";
import "antd/dist/antd.min.css";
import images from "../../assets/static/index";

class Header extends Component {
    static propTypes = {
        userInfo: PropTypes.any,
        logOut: PropTypes.func.isRequired,
    };

    static defaultProps = {
        userInfo: {
            userName: "John Doe",
            url: images.sampleAvatar,
        }
    };

    render() {
        const {
            userInfo,
            logOut,
        } = this.props;

        const userMenu = (
            <Menu>
                <Menu.Item>
                    <a onClick={logOut}>Logout</a>
                </Menu.Item>
            </Menu>
        );

        return (
            <Layout.Header className="header">
                <div/>
                <Dropdown overlay={userMenu}>
                    <span className="user-info">
                        <img className="user-avatar" alt={"user avatar"} src={userInfo.url}/>
                        <span>{userInfo.userName}</span>
                    </span>
                </Dropdown>
            </Layout.Header>
        )
    }
}

export default Header;