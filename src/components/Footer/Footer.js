import React, {Component} from "react";
import "./index.css";
import "antd/dist/antd.min.css";

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <span>Copyright © 2018 Giigaa.com. All rights reserved.</span>
            </div>
        )
    }
}

export default Footer;