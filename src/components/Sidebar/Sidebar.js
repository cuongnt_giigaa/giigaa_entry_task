import React, {Component} from "react";
import {Icon, Layout, Menu} from "antd";
import "antd/dist/antd.min.css";
import "./index.css";
import PropTypes from "prop-types";
import images from "../../assets/static/index";
import {NavLink} from "react-router-dom";
import {withRouter} from "react-router";

const {Sider} = Layout;

class Sidebar extends Component {
    static propTypes = {
        collapsed: PropTypes.bool,
        menuItems: PropTypes.array.isRequired,
        onCollapse: PropTypes.func,
    };

    static defaultProps = {
        collapsed: false,
        onCollapse: () => void(0),
    };

    constructor(props) {
        super(props);
        this.state = {
            collapsed: this.props.collapsed,
        };
    }

    renderMenuItems(menu) {
        if (menu.child) {
            return (
                <Menu.SubMenu
                    key={menu.key}
                    title={
                        <span>
                            <Icon type={menu.icon}/>
                            <span>{menu.name}</span>
                        </span>
                    }
                >
                    {menu.child.map(item => this.renderMenuItems(item))}
                </Menu.SubMenu>
            );
        } else {
            return (
                <Menu.Item key={menu.key}>
                    <NavLink to={menu.url ? menu.url : ""}><span>{menu.name}</span></NavLink>
                </Menu.Item>
            )
        }
    }

    toggle = () => {
        this.setState({collapsed: !this.state.collapsed});
        if (this.props.onCollapse) {
            this.props.onCollapse();
        }
    };

    renderMenu = () => (
        <Menu theme="dark"
              onSelect={this.openItem}
              defaultSelectedKeys={["501"]}
              defaultOpenKeys={["5"]}
              mode="inline">
            <Menu.Item key={1}>
                <span>
                    <Icon type={"laptop"}/>
                    <span>POS</span>
                </span>
            </Menu.Item>
            <Menu.SubMenu
                key={2}
                title={
                    <span>
                        <Icon type={"shopping-cart"}/>
                        <span>Sales</span>
                    </span>
                }
            >
                <Menu.Item key={201}>
                    Order
                </Menu.Item>
            </Menu.SubMenu>
            <Menu.SubMenu
                key={3}
                title={
                    <span>
                        <Icon type={"shop"}/>
                        <span>Purchase</span>
                    </span>
                }
            >
                <Menu.Item key={301}>
                    Purchase Order
                </Menu.Item>
                <Menu.Item key={302}>
                    Vendor
                </Menu.Item>
            </Menu.SubMenu>
            <Menu.SubMenu
                key={4}
                title={
                    <span>
                        <Icon type={"table"}/>
                        <span>Inventory</span>
                    </span>
                }
            >
                <Menu.Item key={401}>
                    Receiving PO
                </Menu.Item>
            </Menu.SubMenu>
            <Menu.SubMenu
                key={5}
                title={
                    <span>
                        <Icon type={"appstore-o"}/>
                        <span>Products</span>
                    </span>
                }
            >
                <Menu.Item key={501}>
                    <NavLink to={"/product/product/list"}><span>Products</span></NavLink>
                </Menu.Item>
            </Menu.SubMenu>
        </Menu>
    );

    render() {
        const {
            menuItems,
        } = this.props;

        const {
            collapsed,
        } = this.state;

        return (
            <Sider
                collapsed={collapsed}
                collapsible={true}
                onCollapse={this.toggle}
            >
                <div>
                    <img className={`logo`}
                         alt={"GiiGaa's Logo"}
                         src={collapsed ? images.giigaaLogoMiniature : images.giigaaLogo}/>
                </div>
                {this.renderMenu()}
            </Sider>
        )
    }
}

export default withRouter(Sidebar);
