import giigaaLogo from "./giigaa_logo.jpg";
import giigaaLogoMiniature from "./giigaa_logo_miniature.jpg";
import sampleAvatar from "./sample-avatar.png";

export default {
    giigaaLogo,
    giigaaLogoMiniature,
    sampleAvatar,
};
