import React from "react";
import {Switch, Route} from "react-router-dom";
import Login from "../pages/login/LoginPage";
import Layout from "../layout/MainLayout";

export default (
    <Switch>
        <Route exact path="/login" component={Login} />
        <Route path="/" component={Layout} />
    </Switch>
);


