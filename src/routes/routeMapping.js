import ProductListPage from "../pages/product/ProductListPage/ProductListPage";
import ProductDetailsPage from "../pages/product/ProductDetailsPage/ProductDetailsPage";

export default  [
    {
        path: "/product/product/list",
        component: ProductListPage,
    },
    {
        path: "/product/product/add",
        component: ProductDetailsPage,
    },
    {
        path: "/product/product/:id/edit",
        component: ProductDetailsPage,
    }
];
