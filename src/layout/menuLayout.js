export default [
    {
        key: 1,
        name: 'POS',
        icon: 'laptop',
    },
    {
        key: 2,
        name: 'Sales',
        icon: 'shopping-cart',
        child: [
            {
                name: 'Order',
                key: 201,
            }
        ]
    },
    {
        key: 3,
        name: 'Purchase',
        icon: 'shop',
        child: [
            {
                name: 'Purchase Order',
                key: 301,
            },
            {
                name: 'Vendor',
                key: 302,
            }

        ]
    },
    {
        key: 4,
        name: 'Inventory',
        icon: 'table',
        child: [
            {
                name: 'Receiving PO',
                key: 401,
            }
        ]
    },
    {
        key: 5,
        name: 'Products',
        icon: 'appstore-o',
        child: [
            {
                name: 'Products',
                key: 501,
                url: '/product/product/list'
            }
        ]
    },
];
