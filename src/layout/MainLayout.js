import React, {Component} from "react";
import {Layout} from "antd";
import "antd/dist/antd.min.css";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import Sidebar from "../components/Sidebar/Sidebar";
import menuItems from "./menuLayout";
import routeMapping from "../routes/routeMapping";
import {Route} from "react-router-dom";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {logOut} from "../pages/login/authActions";
import {getLocalStoredAccessToken} from "../utils/accessToken";

class MainLayout extends Component {
    requireLogin() {
        if (!getLocalStoredAccessToken()) {
            this.props.history.push("/login");
        }
    }

    componentWillReceiveProps() {
        this.requireLogin();
    }

    componentWillMount() {
        this.requireLogin();
    }

    componentDidMount() {
        //this.props.history.push("/product/product/list");
    }

    render() {
        const {
            logOut,
        } = this.props;
        return (
            <Layout className="ant-layout-has-sider" style={{height: "100vh"}}>
                <Sidebar menuItems={menuItems}/>
                <Layout>
                    <Header logOut={logOut}/>
                    <div style={{width: "100%", height: "100%"}}>
                        {routeMapping.map((route, index) =>
                            <Route exact
                                   key={index}
                                   path={route.path}
                                   component={route.component}
                            />)}
                    </div>
                    <Footer/>
                </Layout>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => {
    const {auth} = state;
    return {
        auth,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        logOut: () => dispatch(logOut()),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainLayout));
