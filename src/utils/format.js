function addPaddingZeroes(number, zeroCount = 2) {
    let str = `${number}`;
    if (str.length > zeroCount) {
        return str;
    } else {
        return "0".repeat(zeroCount - str.length) + str;
    }
}


export function formatDate(dateString) {
    const d = new Date(dateString);
    const date = d.getDate();
    const month = d.getMonth() + 1;
    const fullYear = d.getFullYear();
    const hours = d.getHours();
    const minutes = d.getMinutes();
    return `${addPaddingZeroes(date)}/${addPaddingZeroes(month)}/${addPaddingZeroes(fullYear)} ${addPaddingZeroes(hours)}:${addPaddingZeroes(minutes)}`;
}

export function capitaliseFirstCharacter(str) {
    return `${str[0]}`.toUpperCase() + str.slice(1).toLowerCase();
}

export function formatPrice(price) {
    if (typeof (price) !== "number") {
        throw new TypeError("Price must be a number");
    }
    const numberFormatter = new Intl.NumberFormat("en-US");
    return numberFormatter.format(price);
}

