import {BASE_URL} from "../configs";

export default class Request {
    static buildQueryString(criteria) {
        let queryString = "?";
        for (let key in criteria) {
            if (criteria.hasOwnProperty(key)) {
                const value = `${criteria[key]}`;
                if (value !== "" && value !== "ALL" && value !== "-1") {
                    if (queryString !== "?")
                        queryString += "&";
                    queryString += `${key}=${value}`;
                }
            }
        }
        return queryString;
    }

    static async searchProduct(url, criteria, accessToken) {
        try {
            const queryString = Request.buildQueryString(criteria);

            console.log("Request URL: %s", `${BASE_URL + url}${queryString}`);

            const response = await fetch(`${BASE_URL + url}${queryString}`, {
                method: "get",
                headers: new Headers({
                    "X-GIIGAA-ACCESS-TOKEN": accessToken,
                }),
            });

            return await response.json();
        } catch (e) {
            return {
                result: (e.message ? e.message : "Cannot search with the given criteria"),
                reply: null,
            };
        }
    }

    static createFormData(object) {
        let formData = new FormData();
        for (let key in object) {
            if (object.hasOwnProperty(key)) {
                formData.append(key,  object[key]);
            }
        }
        return formData;
    }

    static async postFormData(url, data, accessToken) {
        try {
            console.log("Request.postFormData: (url = %s)", url);
            console.log("data to post: ", data);

            const response = await fetch(BASE_URL + url, {
                method: "post",
                headers: new Headers({
                    "X-GIIGAA-ACCESS-TOKEN": accessToken,
                }),
                body: Request.createFormData(data),
            });

            return await response.json();
        } catch (e) {
            return {
                result: (e.message ? e.message : "Cannot add this product"),
                reply: null,
            }
        }
    }

    static async loadVendorsList(url, data) {
        const {accessToken} = data;
        try {
            const response = await fetch(BASE_URL + url, {
                method: "get",
                headers: new Headers({
                    "X-GIIGAA-ACCESS-TOKEN": accessToken,
                }),
            });
            return await response.json();
        } catch (e) {
            return {
                result: (e.message ? e.message : "Cannot load vendors list"),
                reply: null,
            };
        }
    }

    static async login(url, data) {
        try {
            console.log("Request login to %s with data (%s)", BASE_URL + url, JSON.stringify(data));

            const response = await fetch(BASE_URL + url, {
                method: "post",
                body: Request.createFormData(data),
            });

            return await response.json();
        } catch (e) {
            return {
                result: (e.message ? e.message : "Logging in failed"),
                reply: null,
            };
        }
    }

    static async logout(url, data) {
        let form = new FormData();
        form.set("token", data.accessToken);

        await fetch(BASE_URL + url, {
            method: "post",
            body: form,
        });
    }
}

