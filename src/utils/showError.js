import {message} from "antd";

export function  showError(error) {
    if (error) {
        if (typeof(error) === "string") {
            message.error(error);
        } else {
            message.config({maxCount: 1});
            for (let e in error) {
                if (error.hasOwnProperty(e)) {
                    message.error(`${e}: ${error[e]}`);
                }
            }
        }
    }
}

