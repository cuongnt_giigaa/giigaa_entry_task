import {LOCAL_STORED_ACCESS_TOKEN} from "../constants/auth";


export const getLocalStoredAccessToken = () => window.localStorage.getItem(LOCAL_STORED_ACCESS_TOKEN);
