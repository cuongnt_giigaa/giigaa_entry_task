import * as actionType from "../actionType/filterActionType";

const initState = {
    error: false,
    waiting: false,
    vendorList: [],
};

export default function reducer(state = initState, action) {
    switch (action.type) {
        case actionType.LOAD_ALL_VENDORS_REQUEST: {
            return Object.assign({}, state,  {
                waiting: true,
                error: false,
            });
        }
        case actionType.LOAD_ALL_VENDORS_SUCCEEDED: {
            const {vendorList} = action;
            return Object.assign({}, state, {
                waiting: false,
                error: false,
                vendorList,
            });
        }
        case actionType.LOAD_ALL_VENDORS_FAILED: {
            const {error} = action;
            return Object.assign({}, state, {
                waiting: false,
                error,
            });
        }
        default:
            return state;
    }
}

