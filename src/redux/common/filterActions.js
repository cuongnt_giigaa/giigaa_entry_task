import * as actionType from "../actionType/filterActionType";
import Request from "../../utils/request";
import {getLocalStoredAccessToken} from "../../utils/accessToken";

export const loadAllVendors = () => async (dispatch) => {
    try {
        dispatch({
            type: actionType.LOAD_ALL_VENDORS_REQUEST,
            waiting: true,
            error: false,
        });

        const accessToken = getLocalStoredAccessToken();

        const response = await Request.loadVendorsList("/api/vendor/list", {accessToken});

        if (response.result === "success") {
            dispatch({
                type: actionType.LOAD_ALL_VENDORS_SUCCEEDED,
                waiting: false,
                error: false,
                vendorList: response.reply,
            });
        }  else {
            dispatch({
                type: actionType.LOAD_ALL_VENDORS_FAILED,
                waiting: false,
                error: response.result,
            });
        }
    } catch (e) {
        dispatch({
            type: actionType.LOAD_ALL_VENDORS_FAILED,
            waiting: false,
            error: e.message ? e.message : "Cannot load vendor list",
        });
    }
};
