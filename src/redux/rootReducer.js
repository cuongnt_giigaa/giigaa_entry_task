import {combineReducers} from "redux";
import login_reducers from "../pages/login/authReducers";
import vendor_list_reducers from "./common/filterReducers";
import product_details_reducers from "../pages/product/ProductDetailsPage/productDetailsReducers";
import product_list_reducers from "../pages/product/ProductListPage/productListReducers";

export default combineReducers({
    auth: login_reducers,
    vendor_list: vendor_list_reducers,
    product_details: product_details_reducers,
    product_list: product_list_reducers,
});
