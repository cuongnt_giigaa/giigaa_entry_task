import authActionType from "../../redux/actionType/authActionType";
import {LOCAL_STORED_ACCESS_TOKEN} from "../../constants/auth";

const initState = {
    waiting: false,
    error: false,
    accessToken: null,
};

export default function authReducers(state = initState, action) {
    switch (action.type) {
        case authActionType.USER_LOGIN_REQUEST: {
            return Object.assign({}, state, {
                    waiting: true,
                    error: false,
                }
            );
        }
        case authActionType.USER_LOGIN_SUCCEEDED: {
            const {accessToken} = action;
            window.localStorage.setItem(LOCAL_STORED_ACCESS_TOKEN, accessToken);
            console.log("authReducers = " + accessToken);
            return Object.assign({}, state, {
                    waiting: false,
                    error: false,
                    accessToken: accessToken,
                }
            );
        }
        case authActionType.USER_LOGIN_FAILED:
        case authActionType.USER_LOGOUT_SUCCEEDED: {
            window.localStorage.removeItem(LOCAL_STORED_ACCESS_TOKEN);
            return Object.assign({}, state, {
                    waiting: false,
                    accessToken: null,
                    error: action.error,
                }
            );
        }
        default:
            return state;
    }
}

