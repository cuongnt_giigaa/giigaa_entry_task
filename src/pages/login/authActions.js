import authActionType from "../../redux/actionType/authActionType";
import Request from "../../utils/request";
import md5 from "md5";
import {getLocalStoredAccessToken} from "../../utils/accessToken";

export const logIn = (username, password, history) => async (dispatch) => {
    dispatch({
        type: authActionType.USER_LOGIN_REQUEST,
    });

    try {
        const encryptedPassword = md5(password);

        const response = await Request.login("/api/user/login", {
            username,
            password: encryptedPassword,
        });

        if (response.result !== "success") {
            dispatch({
                type: authActionType.USER_LOGIN_FAILED,
                error: "Please check your username and password",
                waiting: false,
            });
        } else {
            dispatch({
                type: authActionType.USER_LOGIN_SUCCEEDED,
                accessToken: response.reply,
                waiting: true,
            });
            history.push("/");
        }
    } catch (e) {
        dispatch({
            type: authActionType.USER_LOGIN_FAILED,
            error: e.message ? e.message : "Please check your username and password",
            waiting: false,
        });
    }
};

export const logOut = () => async (dispatch) => {
    const accessToken = getLocalStoredAccessToken();
    await Request.logout("/api/user/logout", {
        accessToken,
    });
    dispatch({type: authActionType.USER_LOGOUT_SUCCEEDED});
};