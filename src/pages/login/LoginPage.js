import React, {Component} from "react";
import {Button, Checkbox, Form, Icon, Input, message} from "antd";
import images from "../../assets/static/index";
import "./index.css";
import "antd/dist/antd.min.css";
import {connect} from "react-redux";
import {logIn} from "./authActions";
import {withRouter} from "react-router";
import {getLocalStoredAccessToken} from "../../utils/accessToken";

const FormItem = Form.Item;

class Login extends Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields(async (error, values) => {
            if (!error) {
                await this.props.login(values.userName, values.password, this.props.history);
                if (this.props.auth.error) {
                    message.config({maxCount: 1});
                    message.error(this.props.auth.error);
                }
            }
        });
    };

    componentWillMount() {
        if (getLocalStoredAccessToken()) {
            this.props.history.push("/");
        }
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        const {auth} = this.props;

        return <div className="container">
            <Form onSubmit={this.handleSubmit} className="login-box">
                <img className="logo" alt={"GiiGaa's Logo"} src={images.giigaaLogo}/>
                <FormItem style={{width: "80%"}}>
                    {getFieldDecorator("userName", {
                        rules: [{
                            required: true,
                            message: 'Please enter your user name!'
                        }]
                    })(<Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                              placeholder={"User name"}/>)}
                </FormItem>

                <FormItem style={{width: "80%"}}>
                    {getFieldDecorator("password", {
                        rules: [{
                            required: true,
                            message: 'Please enter your password!'
                        }]
                    })(<Input type={"password"}
                              prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                              placeholder={"Password"}/>)}
                </FormItem>

                <FormItem style={{marginLeft: "10%", marginTop: 0, marginBottom: 0, alignSelf: "flex-start"}}>
                    {getFieldDecorator("remember", {valuePropName: 'checked', initialValue: true})(<Checkbox>Remember
                        me</Checkbox>)}
                </FormItem>

                <div className="vertical-layout">
                    <Button loading={auth.waiting} style={{width: "100%"}} type="primary" htmlType="submit">Log
                        in</Button>
                    <div style={{width: "100%"}}>
                        <a>Register</a>
                        <a style={{float: "right"}}>Forget password</a>
                    </div>
                </div>
            </Form>
        </div>
    }
}

const mapStateToProps = (state) => {
    const {auth} = state;
    return {
        auth,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        login: async (username, password, history) => {
            await dispatch(logIn(username, password, history));
        },
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Form.create()(Login)));
