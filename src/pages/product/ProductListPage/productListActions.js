import * as actionType from "../../../redux/actionType/productActionType";
import {getLocalStoredAccessToken} from "../../../utils/accessToken";
import Request from "../../../utils/request";

export const searchProduct = (criteria) => async (dispatch) => {
    const isOnlyDigits = (key) => !!key.match(/^\d+$/);

    const {key} = criteria;

    if (isOnlyDigits(key)) {
        criteria[key.startsWith("1000") ? "barcode" : "sku"] = key;
    } else {
        criteria["product_name"] = key;
    }

    delete criteria.key;

    criteria["page_size"] = 10000;
    criteria["page_index"] = 1;

    try {
        dispatch({
            type: actionType.PRODUCT_SEARCH_REQUEST,
            waiting: true,
            error: false,
        });

        const accessToken = getLocalStoredAccessToken();
        const result = await Request.searchProduct("/api/product/search", criteria, accessToken);

        if (result.result === "success") {
            dispatch({
                type: actionType.PRODUCT_SEARCH_SUCCEEDED,
                waiting: false,
                error: false,
                items: result.reply,
            });
        } else {
            console.log("productListActions.js/searchProduct:", result);
            dispatch({
                type: actionType.PRODUCT_SEARCH_FAILED,
                waiting: false,
                error: "Search error",
            });
        }
    } catch (e) {
        console.log("productListActions.js/searchProduct:", e);
        dispatch({
            type: actionType.PRODUCT_SEARCH_FAILED,
            waiting: false,
            error: (e.message ? e.message : "Search error"),
        });
    }
};