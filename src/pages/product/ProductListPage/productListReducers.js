import * as actionType from "../../../redux/actionType/productActionType";

const initState = {
    waiting: false,
    error: false,
    items: null,
};

export default function reducer(state = initState, action) {
    switch (action.type) {
        case actionType.PRODUCT_SEARCH_REQUEST: {
            return {
                waiting: true,
                error: false,
                items: null,
            };
        }
        case actionType.PRODUCT_SEARCH_SUCCEEDED: {
            const {items} = action;
            return {
                waiting: false,
                error: false,
                items,
            };
        }
        case actionType.PRODUCT_SEARCH_FAILED: {
            const {error} = action;
            return {
                waiting: false,
                error,
            };
        }

        default:
            return state;
    }
}
