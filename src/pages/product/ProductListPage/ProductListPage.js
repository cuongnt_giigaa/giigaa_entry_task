import React, {Component} from "react";
import {withRouter} from "react-router";
import {Button, Card, Col, Form, Icon, Input, Row, Select, Table, message} from "antd";
import "./index.css";
import {loadAllVendors} from "../../../redux/common/filterActions";
import {searchProduct} from "./productListActions";
import {connect} from "react-redux";
import {showError} from "../../../utils/showError";
import {capitaliseFirstCharacter, formatDate, formatPrice} from "../../../utils/format";

class ProductListPage extends Component {
    productTableColumns = [
        {
            title: 'ID',
            dataIndex: 'id',
        },
        {
            title: 'SKU',
            dataIndex: 'sku',
        },
        {
            title: 'Barcode',
            dataIndex: 'barcode',
        },
        {
            title: 'Name',
            dataIndex: 'product_name',
        },
        {
            title: 'LatestCost',
            dataIndex: 'latest_cost',
            align: "right"
        },
        {
            title: 'Price',
            dataIndex: 'price',
            align: "right",
            render: text => <span>{formatPrice(parseFloat(text))}</span>
        },
        {
            title: 'Status',
            dataIndex: 'product_status',
            render: text => <span>{capitaliseFirstCharacter(text)}</span>
        },
        {
            title: 'Type',
            dataIndex: 'product_type',
            render: text => <span>{capitaliseFirstCharacter(text)}</span>
        },
        {
            title: 'Modified',
            dataIndex: 'updated_time',
            render: text => <span>{formatDate(text)}</span>
        },
        {
            title: 'Action',
            dataIndex: 'action',
            render: (_, record) => <Button icon={"edit"} onClick={() => this.props.history.push(`/product/product/${record.id}/edit`)}/>,
        }
    ];

    componentWillMount() {
        this.props.loadVendorsList();
    }

    componentDidMount() {
        this.props.search({key: "", has_barcode_option: "ALL", vendor_id: "-1", product_status: "ALL", product_type: "ALL"});
    }

    exportData = () => {
        message.info("This has not supported yet");
    };

    submit = async (e) => {
        e.preventDefault();
        this.props.form.validateFields(async (error, value) => {
            await this.props.search(value);
            showError(this.props.products.error);
        });
    };

    renderFilterForm() {
        const {getFieldDecorator} = this.props.form;
        const {vendors, products} = this.props;

        return (
            <Form onSubmit={this.submit} className={"filter-form"}>
                <Row gutter={{md: 10, lg: 10, xl: 10}}>
                    <Col md={3} sm={24}>
                        <Form.Item>
                            {getFieldDecorator("key", {initialValue: ""})(<Input placeholder={"Barcode,SKU,Name"}/>)}
                        </Form.Item>
                    </Col>
                    <Col md={3} sm={24}>
                        <Form.Item>
                            {getFieldDecorator("has_barcode_option", {initialValue: "ALL"})(
                                <Select style={{width: "100%"}}>
                                    <Select.Option value={"ALL"}>All Barcode</Select.Option>
                                    <Select.Option value={"BARCODE_ONLY"}>Barcode Only</Select.Option>
                                    <Select.Option value={"NO_BARCODE"}>No-Barcode</Select.Option>
                                </Select>
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={3} sm={24}>
                        <Form.Item>
                            {getFieldDecorator("vendor_id", {initialValue: "-1"})(
                                <Select style={{width: "100%"}}>
                                    <Select.Option key={"-1"} value={"-1"}>All Vendor</Select.Option>
                                    {vendors.map(vendor => <Select.Option key={vendor.id}
                                                                          value={vendor.id}>{vendor.vendor_name}</Select.Option>)}
                                </Select>
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={3} sm={24}>
                        <Form.Item>
                            {getFieldDecorator("product_status", {initialValue: "ALL"})(
                                <Select style={{width: "100%"}}>
                                    <Select.Option value={"ALL"}>All Status</Select.Option>
                                    <Select.Option value={"ACTIVE"}>Active</Select.Option>
                                    <Select.Option value={"INACTIVE"}>Inactive</Select.Option>
                                </Select>
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={3} sm={24}>
                        <Form.Item>
                            {getFieldDecorator("product_type", {initialValue: "ALL"})(
                                <Select style={{width: "100%"}}>
                                    <Select.Option value={"ALL"}>All Type</Select.Option>
                                    <Select.Option value={"SIMPLE"}>Simple</Select.Option>
                                    <Select.Option value={"CONFIG"}>Config</Select.Option>
                                    <Select.Option value={"ORIGIN"}>Origin</Select.Option>
                                </Select>
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={6} sm={24}>
                        <Button htmlType={"submit"}
                                onClick={this.searchProduct}
                                style={{marginTop: "4px", marginRight: "10px"}}
                                loading={products.waiting}
                                type={"primary"}
                                icon={"search"}>Search</Button>
                        <Button onClick={this.exportData}
                                style={{marginTop: "4px"}}
                                type={"default"}>Export</Button>
                    </Col>
                    <Col md={3} sm={24}>
                        <Button onClick={() => this.props.history.push("/product/product/add")} style={{marginTop: "4px"}} type={"primary"}>
                            <Icon type={"plus"}/> Add Product
                        </Button>
                    </Col>
                </Row>
            </Form>
        )
    }

    render() {
        const {products} = this.props;
        const productItems = products.items ? products.items.map(item => ({key: item.id, ...item})) : [];

        return (
            <div className={"page"}>
                <Card>
                    <div>
                        {this.renderFilterForm()}
                    </div>
                    <Table columns={this.productTableColumns}
                           dataSource={productItems}/>
                </Card>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {
        auth, vendor_list, product_list} = state;
    return {
        auth,
        products: product_list,
        vendors: vendor_list.vendorList,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        async loadVendorsList() {
            await dispatch(loadAllVendors());
        },
        async search(criteria) {
            await dispatch(searchProduct(criteria));
        },
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Form.create()(ProductListPage)));
