import React, {Component} from "react";
import {Button, Card, Col, Form, Icon, Input, message, Row, Select, Tabs} from "antd";
import {withRouter} from "react-router";
import "./index.less";
import {connect} from "react-redux";
import {loadAllVendors} from "../../../redux/common/filterActions";
import {saveProductChanges, addNewProduct, getProductDetails} from "./productDetailsActions";
import {showError} from "../../../utils/showError";

class ProductListPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentId: this.props.match.params.id,
        }
    }

    backToProductList = () => this.props.history.goBack();

    submit = async (e) => {
        let currentProduct = {};

        const {
            saveChanges,
            addProduct,
            currentId,
        } = this.props;

        e.preventDefault();
        this.props.form.validateFields(async (_, data) => {
            for (let key in data) {
                if (data.hasOwnProperty(key)) {
                    const value = data[key];
                    currentProduct[key] = (value ? value : "");
                }
            }

            if (currentId) {
                delete currentProduct.id;
                await addProduct(currentProduct);
            } else {
                currentProduct.product_id = currentProduct.id;
                await saveChanges(currentProduct);
            }

            const {error} = this.props.product_details;
            if (error) {
                showError(error);
            } else {
                message.success("Succeeded!");
                this.backToProductList();
            }
        });
    };

    componentDidMount() {
        const {
            loadVendorsList,
            loadDetail,
        } = this.props;

        const {
            currentId,
        } = this.state;

        loadVendorsList();

        if (currentId) {
            loadDetail(currentId);
        }

        console.log("currentId = " + currentId);
    }

    renderGeneralTab = () => {
        const {getFieldDecorator} = this.props.form;
        const {
            vendors,
            product_details: {product},
        } = this.props;

        const {
            currentId,
        } = this.state;

        let currentProduct = {
            "id": -1,
            "sku": "",
            "barcode": "",
            "vendor_id": "-1",
            "product_name": "",
            "latest_cost": 0,
            "price": 0,
            "product_status": "",
            "product_type": "",
            "updated_time": ""
        };

        if (currentId && product) {
            currentProduct = product;
        }

        return (
            <div>
                <Form.Item label={"Vendor"} labelCol={{span: 2}} wrapperCol={{span: 20}}>
                    {getFieldDecorator("vendor_id", {initialValue: currentProduct.vendor_id})(
                        <Select style={{width: "100%"}}>
                            <Select.Option key={"-1"} value={"-1"}>---------</Select.Option>
                            {vendors.map(vendor => <Select.Option key={vendor.id}
                                                                  value={vendor.id}>{vendor.vendor_name}</Select.Option>)}
                        </Select>)}
                </Form.Item>
                <Form.Item label={"SKU"} labelCol={{span: 2}} wrapperCol={{span: 20}}>
                    {getFieldDecorator("sku", {initialValue: currentProduct.sku})(<Input disabled={true}/>)}
                </Form.Item>
                <Form.Item label={"Barcode"} labelCol={{span: 2}} wrapperCol={{span: 20}}>
                    {getFieldDecorator("barcode", {initialValue: currentProduct.barcode})(<Input placeholder="Barcode"/>)}
                </Form.Item>
                <Form.Item label={"Name"} labelCol={{span: 2}} wrapperCol={{span: 20}}>
                    {getFieldDecorator("product_name", {initialValue: currentProduct.product_name})(<Input placeholder=""/>)}
                </Form.Item>
                <Row>
                    <Col md={12}>
                        <Form.Item label={"Price"} labelCol={{span: 4}} wrapperCol={{span: 8}}>
                            {getFieldDecorator("price", {initialValue: currentProduct.price})(<Input placeholder=""/>)}
                        </Form.Item>
                    </Col>
                    <Col md={12}>
                        <Form.Item label={"Last Cost"} labelCol={{span: 4}} wrapperCol={{span: 8}}>
                            {getFieldDecorator("lastest_cost", {initialValue: currentProduct.lastest_cost})(<Input placeholder="" disabled={true}/>)}
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item label={"Status"} labelCol={{span: 2}} wrapperCol={{span: 4}}>
                    {getFieldDecorator("product_status", {initialValue: currentProduct.product_status})(
                        <Select style={{width: "100%"}}>
                            <Select.Option key={"ACTIVE"} value={"ACTIVE"}>Active</Select.Option>
                            <Select.Option key={"INACTIVE"} value={"INACTIVE"}>Inactive</Select.Option>
                        </Select>)}
                </Form.Item>
                <Form.Item label={"Type"} labelCol={{span: 2}} wrapperCol={{span: 4}}>
                    {getFieldDecorator("product_type", {initialValue: currentProduct.product_type})(
                        <Select style={{width: "100%"}}>
                            <Select.Option key={"SIMPLE"} value={"SIMPLE"}>Simple</Select.Option>
                            <Select.Option key={"CONFIG"} value={"CONFIG"}>Config</Select.Option>
                            <Select.Option key={"ORIGIN"} value={"ORIGIN"}>Origin</Select.Option>
                        </Select>)}
                </Form.Item>
            </div>
        );
    };

    renderAttributesTab = () => {
        return (
            <div>
                attributes
            </div>
        );
    };

    render() {
        return (
            <Form onSubmit={this.submit}>
                <Card style={{width: "100%", height: "100%"}}>
                    <Tabs tabBarExtraContent={<Button type={"primary"} htmlType={"submit"}><Icon type={"save"}/>{this.isAddingNewProduct ? "Add" : "Save"}</Button>}>
                        <Tabs.TabPane key={"general"} tab={"General"}>{this.renderGeneralTab()}</Tabs.TabPane>
                        <Tabs.TabPane key={"attributes"} tab={"Attributes"}>{this.renderAttributesTab()}</Tabs.TabPane>
                    </Tabs>
                </Card>
            </Form>

        );
    }
}

const mapStateToProps = (state) => {
    const {auth, vendor_list, product_details} = state;
    return {
        auth,
        product_details,
        vendors: vendor_list.vendorList,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        loadVendorsList: async () => await dispatch(loadAllVendors()),
        saveChanges: async (changes) => await dispatch(saveProductChanges(changes)),
        addProduct: async (newData) => await dispatch(addNewProduct(newData)),
        loadDetail: async (productId) => await dispatch(getProductDetails(productId)),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Form.create()(ProductListPage)));
