import * as actionType from "../../../redux/actionType/productActionType";
import {getLocalStoredAccessToken} from "../../../utils/accessToken";
import Request from "../../../utils/request";
import {BASE_URL} from "../../../configs";

export const addNewProduct = (product) => async (dispatch) => {
    try {
        dispatch({
            type: actionType.PRODUCT_ADD_REQUEST,
            waiting: true,
            error: false,
        });

        const accessToken = getLocalStoredAccessToken();
        const result = await Request.postFormData("/api/product/add", product, accessToken);

        product.id = result.reply.product_id;

        if (result.result === "success") {
            dispatch({
                type: actionType.PRODUCT_ADD_SUCCEEDED,
                product,
                waiting: false,
                error: false,
            });
        } else {
            dispatch({
                type: actionType.PRODUCT_ADD_FAILED,
                product,
                waiting: false,
                error: result.reply,
            });
        }
    } catch (e) {
        console.log("productListAction.js/addNewProduct: ", e);
        dispatch({
            type: actionType.PRODUCT_ADD_FAILED,
            waiting: false,
            error: e.message ? e.message : "Cannot add this product",
        });
    }
};

export const saveProductChanges = (product) => async (dispatch) => {
    try {
        dispatch({
            type: actionType.PRODUCT_SAVE_CHANGES_REQUEST,
            waiting: true,
            error: false,
        });

        const accessToken = getLocalStoredAccessToken();
        const result = await Request.postFormData("/api/product/edit", product, accessToken);

        if (result.result === "success") {
            dispatch({
                type: actionType.PRODUCT_SAVE_CHANGES_SUCCEEDED,
                product,
                waiting: false,
                error: false,
            });
        } else {
            dispatch({
                type: actionType.PRODUCT_SAVE_CHANGES_FAILED,
                product,
                waiting: false,
                error: result.reply,
            });
        }
    } catch (e) {
        console.log("productListAction.js/saveProductChanges: ", e);
        dispatch({
            type: actionType.PRODUCT_SAVE_CHANGES_FAILED,
            waiting: false,
            error: e.message ? e.message : "Cannot save changes for this product",
        });
    }
};

export const getProductDetails = (productId) => async (dispatch) => {
    try {
        dispatch({
            type: actionType.PRODUCT_GET_DETAILS_REQUEST,
            waiting: true,
            error: false,
        });

        const accessToken = getLocalStoredAccessToken();

        const result = await Request.searchProduct("/api/product/search", {product_id: productId, page_index: 1, page_size: 1}, accessToken);
        console.log("productDetailsActions/getProductDetails: result = ", JSON.stringify(result));

        if (result.result === "success") {
            dispatch({
                type: actionType.PRODUCT_GET_DETAILS_SUCCEEDED,
                product: result.reply[0],
                waiting: false,
                error: false,
            });
        } else {
            dispatch({
                type: actionType.PRODUCT_GET_DETAILS_FAILED,
                waiting: false,
                error: result.reply,
            });
        }
    } catch (e) {
        console.log("productListAction.js/saveProductChanges: ", e);
        dispatch({
            type: actionType.PRODUCT_GET_DETAILS_FAILED,
            waiting: false,
            error: e.message ? e.message : "Cannot save changes for this product",
        });
    }

};