import * as actionType from "../../../redux/actionType/productActionType";

const initState = {
    waiting: false,
    error: false,
    product: null,
};

export default function reducer(state = initState, action) {
    switch (action.type) {
        case actionType.PRODUCT_SAVE_CHANGES_REQUEST:
        case actionType.PRODUCT_ADD_REQUEST:
        case actionType.PRODUCT_GET_DETAILS_REQUEST: {
            return {
                ...state,
                waiting: true,
                error: false,
            };
        }
        case actionType.PRODUCT_SAVE_CHANGES_SUCCEEDED:
        case actionType.PRODUCT_ADD_SUCCEEDED: {
            return {
                ...state,
                waiting: false,
                error: false,
            }
        }
        case actionType.PRODUCT_GET_DETAILS_SUCCEEDED: {
            const {product} = action;
            return {
                product,
                waiting: false,
                error: false,
            };
        }
        case actionType.PRODUCT_SAVE_CHANGES_FAILED:
        case actionType.PRODUCT_ADD_FAILED:
        case actionType.PRODUCT_GET_DETAILS_FAILED: {
            const {error} = action;
            return {
                ...state,
                waiting: false,
                error,
            };
        }
        default:
            return state;
    }
}
