import React from 'react';
import ReactDOM from 'react-dom';
import './pages/login/index.css';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from "react-router-dom";
import store from "./redux/store";
import {Provider} from 'react-redux';
import routes from "./routes/routes";

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            {routes}
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
);

registerServiceWorker();
